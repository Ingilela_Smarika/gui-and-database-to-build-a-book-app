import sqlite3

def connect():
    conn=sqlite3.connect("books.db")
    cur=conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS book(id INTEGER PRIMARY KEY,title text,author text,year integer,isbn integer)")
    conn.commit()
    conn.close()

def insert(title,author,year,isbn):
    conn=sqlite3.connect("books.db")
    cur=conn.cursor()
    cur.execute("INSERT INTO book VALUES(NULL,?,?,?,?)",(title,author,year,isbn))
    cur.execute("INSERT INTO book VALUES(NULL,?,?,?,?)",(title,author,year,isbn))
    conn.commit()
    conn.close()

def view():
    conn=sqlite3.connect("books.db")
    cur=conn.cursor()
    cur.execute("SELECT * FROM book")
    rows=cur.fetchall()
    return rows

def delete(id):
    conn=sqlite3.connect('books.db')
    cur=conn.cursor()
    cur.execute("DELETE FROM book WHERE id=?",(id,));
    conn.commit()
    conn.close()

def search(title='',author='',year='',isbn=''):
    conn=sqlite3.connect('books.db')
    cur=conn.cursor()
    cur.execute('SELECT * FROM book WHERE title=? or author=? OR  year=? OR  isbn=? ',(title,author,year,isbn))
    rows=cur.fetchall()
    conn.commit()
    conn.close()
    return rows

def update(id,title,author,year,isbn):
    conn=sqlite3.connect('books.db')
    cur=conn.cursor()
    cur.execute("UPDATE book SET title=?,author=?,year=?,isbn=? where id =?",(title,author,year,isbn,id));
    conn.commit()
    conn.close()

connect()
#insert("The Earth","John Smith",1918,913123132)
#insert("The Sea","Emmanuel",1231,198647654)
#delete(3)
#update(4,"The Sky","Galatians",1223,987098765)
#print(view())
#print(search(author="John Smith"))